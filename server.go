package server

import (
	"html/template"
	"net/http"

	"gitlab.com/jbfrench/go-starter-server/logger"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

/*
	Routing
*/

var middlewareArray []mux.MiddlewareFunc

type Route struct {
	Name        string
	Method      string
	Path        string
	HandlerFunc http.HandlerFunc
}

var routeArray []Route

//Allows users of the library to add routes to the server. This is done this way to enforce inversion of control.
//The services should push the routes to the router. The router shouldn't pull the routes from them.
func RegisterRoute(newRoute Route) {
	logger.Log("Adding route "+newRoute.Name, logger.DEBUG)
	routeArray = append(routeArray, newRoute)
}

func RegisterGlobalMiddleware(fn mux.MiddlewareFunc) {
	middlewareArray = append(middlewareArray, fn)
}

//This will create a new Gorilla Mux router that will call router.Handle and router.Methods on the routeArray variable in this file.
//It also will set up any global middleware defined through AddGlibalMiddleware
func NewRouter(corsOptionsParam *cors.Options) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	logger.Log("Adding routes", logger.ALL)
	for _, route := range routeArray {
		logger.Log("Configuring router with route: "+route.Name, logger.DEBUG)
		router.Methods(route.Method).Path(route.Path).Handler(route.HandlerFunc)
	}
	globalMiddlewareConfig(router, corsOptionsParam)
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("public/"))))
	return router
}

//This method allows n number of middleware handlers to be applied to ALL of the handlers in the routeArray from this file.
func globalMiddlewareConfig(router *mux.Router, corsOptionsParam *cors.Options) {

	if corsOptionsParam != nil {
		corsOptions := corsOptionsParam
		c := cors.New(*corsOptions)
		router.Use(c.Handler)
	} else {
		router.Use(cors.Default().Handler)
	}

	router.Use(logger.RequestLogger)

	for _, f := range middlewareArray {
		router.Use(f)
	}
}

/*
	Templating
*/

//The templates variable is filled in the ConfigureTemplates method from this file.
var templates *template.Template

//This function takes a URI and uses that as the base location to parse all of your html templates.
func ConfigureTemplates(templateFolderUri string) {
	templates = template.Must(template.New("main").ParseGlob(templateFolderUri))
}

//This is a handler that will take a template name and appends the .html extention. It will then serve the html template.
//If you pass a model, it will attempt to use that model to fill out the template when it is parsed.
func RenderTemplate(w http.ResponseWriter, template string, model interface{}) {
	err := templates.ExecuteTemplate(w, template+".html", model)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
