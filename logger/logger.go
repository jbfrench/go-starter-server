package logger

import (
	"log"
	"net/http"
	"time"
)

type LEVEL int

var logLevel LEVEL

const (
	ERROR LEVEL = iota
	DEBUG
	INFO
	ALL
)

func init() {
	logLevel = ALL
}

func SetLogLevel(level LEVEL) {
	logLevel = level
}

func RequestLogger(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		inner.ServeHTTP(w, r)
		if logLevel > ERROR {
			log.Printf("%s\t%s\t%s", r.Method, r.RequestURI, time.Since(start))
		}
	})
}

func Log(content interface{}, level LEVEL) {
	if logLevel >= level {
		start := time.Now()
		log.Printf("%s\t%s", content, time.Since(start))
	}
}
